import * as mocha from 'mocha';
import * as chai from 'chai';
import chaiHttp = require('chai-http');

import app from '../src/app';
import {MongoClient} from 'mongodb';
import config from '../src/config';

chai.use(chaiHttp);
const expect = chai.expect;
const questions_nums = [1, 2, 3, 4, 5];

describe('Questions', () => {
  let mydb;
  //create test questions
  before((done) => {
    MongoClient.connect(config.get('connection')).then(db => {
      mydb = db;
      db.collection('questions').insert(questions_nums.map(num => {
        return {
          id      : num,
          text    : `Question number ${num}?`,
          answers : [
            {
              id: 1,
              text: 'Answer 1'
            }, {
              id: 2,
              text: 'Answer 2'
            }, {
              id: 3,
              text: 'Answer 3'
            }, {
              id: 4,
              text: 'Answer 4'
            }
          ]
        }
      })).then(() => done());
    });
  });

  it('Get all questions', () => {
    return chai.request(app).get('/questions')
      .then(res => {
        expect(res.status).to.equal(200);
        expect(res).to.be.json;
        expect(res.body).to.be.an('array');
      });
  });

  it('Get question by id = 1', () => {
    return chai.request(app).get('/questions/1')
      .then(res => {
        let question = res.body;
        expect(question).to.exist;
        expect(question).to.have.all.keys([
          'id',
          'text',
          'answers'
        ]);
      });
  });

  it('Add new question', () => {
    return chai.request(app)
      .post('/questions')
      .send({
        id: 1,
        text: 'Question 1?',
        answers: [
          {
            id: 1,
            text: 'Answer 1'
          }, {
            id: 2,
            text: 'Answer 2'
          }, {
            id: 3,
            text: 'Answer 3'
          }
        ]
      })
      .then(res => {
        expect(res.status).to.equal(200);
      });
  });

  it('Send answer and get next question', () => {
    return chai.request(app)
      .post('/questions/1')
      .send({
        id: 2,
        text: 'Answer 2'
      })
      .then(res => {
        const nextQuestion = res.body;
        expect(res.status).to.equal(200);
        expect(nextQuestion).to.have.all.keys([
          'id',
          'text',
          'answers'
        ]);
        expect(nextQuestion).to.have.property('id', 3); //setting next question in config.json
      });
  });

  //remove test questions
  after((done) => {
    mydb.collection('questions').remove({id: {$in: questions_nums}}).then(() => {
      mydb.close();
      done();
    });
  })

});