import {MongoClient, Collection} from 'mongodb';
import config from '../config';
import Question from '../interfaces/Question';

class QuestionRepository {
  private questions;

  constructor() {
    this.openConnection();
  }

  private async openConnection() {
     const db = await MongoClient.connect(config.get('connection'));
     this.questions = db.collection('questions');
  }

  public async getAllQuestions() {
    return await this.questions.find().toArray();
  }

  public async getQuestionById(id: number) {
    return await this.questions.findOne({id}, {_id: 0});
  }

  public async addQuestion(question: Question) {
    await this.questions.update(
      {id: question.id},
      {$set: question},
      {upsert: true}
    );
    return question;
  }

  public async updateQuestion(id: number, question: any) {
    await this.questions.update(
      {id: question.id},
      {$set: question}
    );
    return this.getQuestionById(id);
  }
}

export default new QuestionRepository();