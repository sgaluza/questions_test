import Answer from './Answer';

interface Question {
  id      : number,
  text    : string,
  answers : Array<Answer>
}

export default Question;