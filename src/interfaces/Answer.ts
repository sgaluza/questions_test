interface Answer {
  id    : number,
  text  : string;
}

export default Answer;