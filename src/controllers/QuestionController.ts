import {JsonController, Param, Body, Get, Post} from "routing-controllers";
import Answer from '../interfaces/Answer';
import Question from '../interfaces/Question';

import questionRepository from '../repositories/QuestionRepository';

const Sequence = require('../sequence');

@JsonController()
export class QuestionController {
    @Get("/questions")
    getAllQuestions() {
        return questionRepository.getAllQuestions();
    }

    @Get("/questions/:id")
    getQuestion(@Param("id") id: number) {
        return questionRepository.getQuestionById(id);
    }

    @Post("/questions")
    addQuestion(@Body() question: Question) {
        return questionRepository.addQuestion(question);
    }

    @Post("/questions/:id")
    nextQuestion(@Param("id") id: number, @Body() answer: Answer) {
        let next_question;

        const question_config = Sequence.find(s => s.question === id);
        if (question_config) {
            for (let sequence of question_config.next) {
                if (sequence.answers.includes(answer.id)) {
                    next_question = sequence.question;
                }
            }
        }

        if (!next_question) {
            next_question = id + 1;
        }

        return questionRepository.getQuestionById(next_question);
    }

}