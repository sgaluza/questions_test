import "reflect-metadata";
import {createExpressServer} from "routing-controllers";
import {QuestionController} from "./controllers/QuestionController";

const app = createExpressServer({
  controllers: [
    QuestionController
  ]
});

app.listen(3000, () => console.log('Server listening at localhost:3000'));

export default app;